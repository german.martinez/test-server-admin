FROM python:3.8
RUN pip install flask flask-cors flask-restx
ENV FLASK_APP=/app/main.py
ENV FLASK_ENV=development
COPY . /app
WORKDIR /app
ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]
