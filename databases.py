import sqlite3
from enum import Enum
from sqlite3 import OperationalError


class Schemas(Enum):
    SENSOR = """CREATE TABLE SENSOR (
                ID INTEGER PRIMARY KEY, 
                SENSOR_NAME VARCHAR(256) UNIQUE, 
                SENSOR_TYPE INT,
                MEASUREMENT_UNIT INT, 
                LOCATION INT, 
                PROTOCOL INT, 
                GEOLOCATION VARCHAR(256)
            )"""

    SENSOR_TYPE = """CREATE TABLE SENSOR_TYPE (
                ID INTEGER PRIMARY KEY, 
                LABEL VARCHAR(256) UNIQUE, 
                VALUE INT
            )"""

    MEASUREMENT_UNIT = """CREATE TABLE MEASUREMENT_UNIT (
                ID INTEGER PRIMARY KEY, 
                LABEL VARCHAR(256) UNIQUE, 
                VALUE INT
            )"""

    LOCATION = """CREATE TABLE LOCATION (
                ID INTEGER PRIMARY KEY, 
                LABEL VARCHAR(256) UNIQUE,
                VALUE INT
            )"""

    PROTOCOL = """CREATE TABLE PROTOCOL (
                ID INTEGER PRIMARY KEY, 
                LABEL VARCHAR(256) UNIQUE, 
                VALUE INT
            )"""


class Database:
    def __init__(self, db_file_path="sensors.sqlite"):
        self.db_file_path = db_file_path

    def get_connection(self):
        return sqlite3.connect(self.db_file_path)

    def create_tables(self, drop=False):
        conn = self.get_connection()
        cursor = conn.cursor()
        try:
            for schema in Schemas:
                try:
                    cursor.execute(schema.value)
                    print(f"table {schema.name} created!!")
                except OperationalError as e:
                    if drop and "already exist" in e.args[0]:
                        # cursor.execute(f"DROP TABLE IF EXISTS {schema.name}")
                        print(f"table {schema.name} DROPPED!!")
        except Exception as e:
            conn.commit()
            conn.close()

        print("All tables created!!")




if "__main__" == __name__:
    Database().create_tables()
