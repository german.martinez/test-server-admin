from flask import Flask, request
from _sqlite3 import IntegrityError

from flask_cors import CORS
from flask_restx import Api, fields, Resource

from models import Sensor

app = Flask(__name__)
CORS(app, origins="*")

api = Api(
    app, title="sensor-management-api", description="A Sensor Crud API"
)

ns = api.namespace("sensors", description="Sensors Operations",)

sensor = api.model(
    "Sensor",
    {
        "id": fields.Integer(readonly=True, description="Sensor Unique Identifier"),
        "SensorName": fields.String(required=True, description="Senor Unique Name"),
        "SensorType": fields.String(required=True, description="Senor Type"),
        "MeasurementUnit": fields.String(
            required=True, description="Measurement Unit related to Sensor"
        ),
        "Location": fields.String(
            required=True, description="Location Name of the sensor"
        ),
        "Protocol": fields.String(required=True, description="Communication Protocol"),
        "Geolocation": fields.String(required=False, description="Coordinates"),
    },
)

options = api.model('Options',  {
            "value": fields.Integer(required=True),
            "label": fields.String(required=True),
        })

sensorCreate = api.model(
    "SensorCreate",
    {
        "id": fields.Integer(rßeadonly=True, description="Sensor Unique Identifier"),
        "SensorName": fields.String(required=True),
        "SensorType": fields.Nested(options),
        "MeasurementUnit": fields.Nested(options),
        "Location": fields.Nested(options),
        "Protocol": fields.Nested(options),
        "Geolocation": fields.String(required=False, description="Coordinates"),
    },
)


@app.route("/health-check")
def health_check():
    return {"status": "ok"}


@ns.route("/")
class SensorList(Resource):
    @ns.doc("create-sensors")
    @ns.expect(sensorCreate)
    @ns.marshal_with(sensorCreate, code=201)
    def post(self):
        try:
            Sensor().deserializer(api.payload).save()
        except IntegrityError:
            return request.json, 422
        except Exception as e:
            return request.json, 500

        return {"status": "saved"}

    @ns.doc("get-sensors")
    @ns.marshal_list_with(sensor)
    def get(self):
        response = [sensor.serializer() for sensor in Sensor().get_all()]
        return response


@ns.route("/validate-name/<string:sensor_name>")
@ns.param("sensor_name")
class ValidateName(Resource):
    @ns.doc("validate-name")
    def get(self, sensor_name):
        is_valid = {"isValid": True}
        try:
            sensor_exist = Sensor().get_by(sensor_name=sensor_name).first()
        except Exception as e:
            return {"status": False}, 500

        if sensor_exist:
            is_valid["isValid"] = False

        return is_valid


@ns.route("/search/<string:criteria>")
@ns.param("criteria")
class SensorList(Resource):

    @ns.doc("search-sensors")
    @ns.marshal_list_with(sensor)
    def get(self, criteria):
        response = [sensor.serializer() for sensor in Sensor().get_by('like', sensor_name=criteria).all()]
        return response
