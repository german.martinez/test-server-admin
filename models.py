from dataclasses import dataclass
from databases import Database

@dataclass
class BaseModel:
    TABLE_NAME = "BASE_MODEL"

    def __init__(self):
        self.resul_set = None
        self.conn = None

    def _get_values(self) -> tuple:
        """
        Private method to get Model attributes like a tuple
        :return: tuple
        """
        return tuple(i[1] for i in self.__dict__.items() if i[0] != "id")

    def save(self) -> None:
        """
        Persist object in database
        """

        sql = f"""
            INSERT INTO {self.TABLE_NAME} ({",".join([ i.upper() for i in self.__dict__.keys() if i != "id"])})
            VALUES ({",".join(["?"]*(len(self.__dict__.keys()) - 1 ))})
        """
        conn = Database().get_connection()
        try:
            conn.execute(sql, self._get_values())
            conn.commit()
        except Exception as e:
            raise(e)
        finally:
            conn.close()

    def get_all(self) -> list:
        """
        Retrive all occurences of the model from the database
        :return: list
        """

        sql = f"""
               SELECT * FROM {self.TABLE_NAME}
           """
        conn = Database().get_connection()
        try:
            res = conn.execute(sql).fetchall()
        except Exception as e:
            raise(e)
        finally:
            conn.close()
        clazz = type(self)
        _res = []
        for i in res:
            _res.append(clazz(*i))
        return _res

    def get_by(self, *args, **kargs):
        if 'like' in args:
            where_clause = "OR ".join([f"{k[0].upper()} like '%{k[1]}%'" for k in kargs.items()])
        else:
            where_clause = "AND ".join([f"{k[0].upper()} = '{k[1]}'" for k in kargs.items()])

        sql = f"SELECT * FROM {self.TABLE_NAME} WHERE {where_clause}"

        self.conn = Database().get_connection()
        self.resul_set = self.conn.execute(sql)

        return self

    def first(self):
        try:
            instance = self.resul_set.fetchone()
            clazz = type(self)
        except Exception as e:
            self.conn.close()
            raise(e)
        finally:
            self.conn.close()
        return clazz(*instance) if instance else None

    def all(self):
        result = []
        try:
            instances = self.resul_set.fetchall()
            clazz = type(self)
        except Exception as e:
            self.conn.close()
            raise(e)
        finally:
            self.conn.close()

        for instance in instances:
           result.append(clazz(*instance))
        return result

    def deserializer(self, data):
        pass

    def serializer(self):
        pass

@dataclass
class Sensor(BaseModel):
    TABLE_NAME = "SENSOR"

    id: int = None
    sensor_name: str = None
    sensor_type: int = None
    measurement_unit: int = None
    location: int = None
    protocol: int = None
    geolocation: str = None

    def deserializer(self, data):
        self.sensor_name = data.get("SensorName")
        self.sensor_type = data.get("SensorType").get("value")
        self.measurement_unit = data.get("MeasurementUnit").get("value")
        self.location = data.get("Location").get("value")
        self.protocol = data.get("Protocol").get("value")
        self.geolocation = data.get("Geolocation")
        return self

    def serializer(self):
        sensor = {}
        sensor["id"] = self.id
        sensor["SensorName"] = self.sensor_name
        sensor["SensorType"] = SensorType().get_by(id=self.sensor_type).first().label
        sensor["MeasurementUnit"] = MeasurementUnit().get_by(id=self.measurement_unit).first().label
        sensor["Location"] = Location().get_by(id=self.location).first().label
        sensor["Protocol"] = Protocol().get_by(id=self.protocol).first().label
        sensor["Geolocation"] = self.geolocation

        return sensor

@dataclass
class BaseTypeModel(BaseModel):
    id: int = None
    label: str = None
    value: str = None


class SensorType(BaseTypeModel):
    TABLE_NAME = "SENSOR_TYPE"


class MeasurementUnit(BaseTypeModel):
    TABLE_NAME = "MEASUREMENT_UNIT"


class Location(BaseTypeModel):
    TABLE_NAME = "LOCATION"


class Protocol(BaseTypeModel):
    TABLE_NAME = "PROTOCOL"

